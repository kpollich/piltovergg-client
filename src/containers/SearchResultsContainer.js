import React, { Component } from 'react'
import { connect } from 'react-redux'

import SummonerContainer from './SummonerContainer'
import StatsContainer from './StatsContainer'

import { fetchSummonerData, fetchSummonerStats } from '../actions'

class SearchResultsContainer extends Component {
  componentDidMount () {
    const { summoner, region } = this.props.routeParams
    const { dispatch } = this.props
    
    dispatch(fetchSummonerData({ summoner, region }))
      .then(({ summonerData }) => {
        const summonerId = summonerData[summoner].id
        dispatch(fetchSummonerStats({ summonerId, region }))
      })
  }
  
  render () {
    const { summoner, region } = this.props.routeParams
    return (
      <div className="search-results-container">
        <SummonerContainer
          region={region}
          summoner={summoner}
        />
        <StatsContainer 
          region={region}
          summoner={summoner}
        />
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    summoner: state.summoner,
    region: state.region
  }
}

export default connect(mapStateToProps)(SearchResultsContainer)