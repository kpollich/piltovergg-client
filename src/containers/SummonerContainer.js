import React, { Component } from 'react'
import { connect } from 'react-redux'

import Summoner from '../components/Summoner'

class SummonerContainer extends Component {
  render () {
    const { isFetching, summoner, data } = this.props

    if (isFetching || !data ) {
      return (
        <div className="a-spinner-eventually"></div>
      )
    }

    return (
      <div className="summoner-container">
        <Summoner summonerData={data[summoner]} />
      </div>
    )
  }
}

function mapStateToProps (state) {
  return {
    data: state.summonerData,
    isFetching: state.isFetching,
  }
}

export default connect(mapStateToProps)(SummonerContainer)
