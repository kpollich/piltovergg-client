import React, { Component } from 'react'

class Summoner extends Component {
  render () {
    const summonerInfo = this.props.summonerData

    const profileImageId = summonerInfo.profileIconId
    const profileIconSrc = `http://ddragon.leagueoflegends.com/cdn/6.24.1/img/profileicon/${profileImageId}.png`

    return (
      <div className="summoner">
        <img src={profileIconSrc} alt="Profile Icon" className="profile-icon" />
        <div className="summoner-info">
          <h2 className="summoner-name">{summonerInfo.name}</h2>
          <span className="summoner-level">Level {summonerInfo.summonerLevel}</span>
        </div>
      </div>
    )
  }
}

export default Summoner
