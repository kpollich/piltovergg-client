import React, { Component } from 'react'

class Stats extends Component {
  render () {
    const { queueType, wins } = this.props

    return (
      <div className="stats-card">
        <p><strong>{queueType} Wins: </strong>{wins}</p>
      </div>
    )
  }
}

export default Stats
