export const setSummonerData = (data) => {
  return {
    type: 'SET_SUMMONER_DATA',
    data
  }
}

export const setStatsData = (data) => {
  return {
    type: 'SET_STATS_DATA',
    data
  }
}

export const requestSummonerData = (summoner, region) => {
  return {
    type: 'REQUEST_SUMMONER_DATA',
    summoner,
    region
  }
}

export const receiveSummonerData = (summoner, region, summonerData) => {
  return {
    type: 'RECEIVE_SUMMONER_DATA',
    summoner,
    region,
    summonerData,
    receivedAt: Date.now()
  }
}

export const requestSummonerStats = (summonerId, region) => {
  return {
    type: 'REQUEST_SUMMONER_STATS',
    summonerId,
    region
  }
}

export const receiveSummonerStats = (summonerId, region, summonerStats) => {
  return {
    type: 'RECEIVE_SUMMONER_STATS',
    summonerId,
    region,
    summonerStats,
    receivedAt: Date.now()
  }
}

export const fetchSummonerData = ({ summoner, region }) => {
  return (dispatch) => {
    dispatch(requestSummonerData(summoner, region))

    return fetch(`${process.env.REACT_APP_API_HOST}/summoners/${region}/${summoner}`)
      .then((response) => response.json())
      .then((data) => dispatch(receiveSummonerData(summoner, region, data)))
  }
}

export const fetchSummonerStats = ({ summonerId, region }) => {
  return (dispatch) => {
    dispatch(requestSummonerStats(summonerId, region))

    return fetch(`${process.env.REACT_APP_API_HOST}/stats/${region}/${summonerId}`)
      .then((response) => response.json())
      .then((data) => dispatch(receiveSummonerStats(summonerId, region, data)))
  }
}
